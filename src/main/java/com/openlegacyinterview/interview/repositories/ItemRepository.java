package com.openlegacyinterview.interview.repositories;

import com.openlegacyinterview.interview.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of Item
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
}

package com.openlegacyinterview.interview.models;

import com.openlegacyinterview.interview.configs.Exceptions.ItemAmountNegativeException;

import javax.persistence.*;

/**
 * Model of Item
 * Normally, DTO and DAO and converters need to be implemented.
 * Since out scope is simple, just use one model
 */
@Entity(name="ITEMS")
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", nullable=false, unique=true, length=40)
    private String name;
    @Column(name="amount", nullable=false)
    private int amount;
    @Column(name="code", nullable=false, unique=true)
    private String inventoryCode;
    @Column(name="details", length=200)
    private String details;

    public Item() {
        super();
    }

    public Item(Long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        if (amount < 0)
            throw new ItemAmountNegativeException();
        this.amount = amount;
    }

    public String getInventoryCode() {
        return inventoryCode;
    }

    public void setInventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}

package com.openlegacyinterview.interview.controllers;

import com.openlegacyinterview.interview.configs.Exceptions.ItemNotFoundException;
import com.openlegacyinterview.interview.models.Item;
import com.openlegacyinterview.interview.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

/**
 * Rest API Controller
 */
@RestController
@RequestMapping("/items")
public class ItemController {

    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/getAll")
    public List<Item> retrieveAllItems() {
        return itemService.list();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Object> retrieveItem(@PathVariable long id) {
        Optional<Item> item = itemService.get(id);

        if (!item.isPresent())
            throw new ItemNotFoundException("id-" + id);

        return ResponseEntity.ok(item.get());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteItem(@PathVariable long id) {
        Optional<Item> item = itemService.get(id);
        if (!item.isPresent())
            throw new ItemNotFoundException("id-" + id);

        itemService.delete(id);
        return ResponseEntity.ok("Item id-" + id + " is deleted.");
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createItem(@RequestBody Item item) {
        Item saved = itemService.save(item);

        return ResponseEntity.ok(saved);

    }

    @PutMapping("/withdraw/{id}/quantity/{quantity}")
    public ResponseEntity<Object> withdrawItem(@PathVariable long id, @PathVariable int quantity) {
        Optional<Item> itemOptional = itemService.get(id);
        if (!itemOptional.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(itemService.withdraw(itemOptional.get(), quantity));
    }

    @PutMapping("/deposit/{id}/quantity/{quantity}")
    public ResponseEntity<Object> depositItem(@PathVariable long id, @PathVariable int quantity) {
        Optional<Item> itemOptional = itemService.get(id);
        if (!itemOptional.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(itemService.deposit(itemOptional.get(), quantity));
    }
}

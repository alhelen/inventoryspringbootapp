package com.openlegacyinterview.interview.configs;

import com.openlegacyinterview.interview.configs.Exceptions.ItemAmountNegativeException;
import com.openlegacyinterview.interview.configs.Exceptions.ItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This Class defines the exception handling classes.
 **/
@ControllerAdvice
public class ItemExceptionController {
    @ExceptionHandler(value = ItemNotFoundException.class)
    public ResponseEntity<Object> itemNotFoundExceptionException(ItemNotFoundException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ItemAmountNegativeException.class)
    public ResponseEntity<Object> itemAmountNegativeExceptionException(ItemAmountNegativeException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
}


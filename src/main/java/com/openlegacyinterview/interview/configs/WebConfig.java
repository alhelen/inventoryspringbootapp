package com.openlegacyinterview.interview.configs;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *  Class to handle rest api config
 */
@Configuration
@EnableJpaRepositories("com.openlegacyinterview.interview.repositories")
@EntityScan("com.openlegacyinterview.interview.models")
public class WebConfig{
    // TODO: Can override configure() to specify the tranfic handling and security.

}

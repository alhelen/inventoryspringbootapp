package com.openlegacyinterview.interview.configs.Exceptions;

public class ItemNotFoundException extends RuntimeException {

    public ItemNotFoundException(String id) {
        super("Item id not found : " + id);
    }

}

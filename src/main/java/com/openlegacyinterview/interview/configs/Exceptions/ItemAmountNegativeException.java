package com.openlegacyinterview.interview.configs.Exceptions;

public class ItemAmountNegativeException extends RuntimeException {

    public ItemAmountNegativeException() {
        super("Item amount cannot be less than 0!");
    }
}

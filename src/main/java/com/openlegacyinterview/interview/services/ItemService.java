package com.openlegacyinterview.interview.services;

import com.openlegacyinterview.interview.models.Item;
import com.openlegacyinterview.interview.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service class of Entity
 * The Hierarchy is normally, ItemService is an interface and ItemServiceImpl will implement this interface.
 * Since our scope is simple, will skip to use ItemService only.
 */
@Service
public class ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> list() {
        return itemRepository.findAll();
    }

    public Optional<Item> get(long id) {
        return itemRepository.findById(id);
    }

    public void delete(long id) {
        itemRepository.deleteById(id);
    }

    public Item save(Item item) {
        return itemRepository.save(item);
    }

    public Item withdraw(Item item, int quantity) {
        item.setAmount(item.getAmount() - quantity);
        return itemRepository.save(item);
    }

    public Item deposit(Item item, int quantity) {
        item.setAmount(item.getAmount() + quantity);
        return itemRepository.save(item);
    }
}

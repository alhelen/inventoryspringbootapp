package com.openlegacyinterview.interview.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.openlegacyinterview.interview.configs",
                                "com.openlegacyinterview.interview.controllers",
                                "com.openlegacyinterview.interview.models",
                                "com.openlegacyinterview.interview.repositories",
                                "com.openlegacyinterview.interview.services"})
public class InterviewInventoryAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewInventoryAppApplication.class, args);
    }

}

FROM java:8
VOLUME /api
ADD target/interview-0.0.1-SNAPSHOT.jar api.jar
RUN bash -c 'touch /api.jar'
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /api.jar"]
